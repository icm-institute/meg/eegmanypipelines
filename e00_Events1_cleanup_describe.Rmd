---
title: Events name cleanup and describe
output: 
  html_notebook: 
    toc: yes
    code_folding: hide
  html_document: 
    toc: yes
---

```{r, echo=F, message=F}
library(tidyverse)
library(MaxPac)
library(ggbeeswarm)
library(patchwork)
library(flextable)
theme_set(theme_minimal())
dirRaw <- '/home/maximilien.chaumon/liensNet/analyse/meeg/EEGMANYPIPELINES/rawdata'

```

## Cleaning up event names in BIDS data

```{r CleanupBIDS}

TriggerTable = read_csv('/home/maximilien.chaumon/liensNet/analyse/meeg/EEGMANYPIPELINES/sourcedata/documentation/TriggerTable.csv', 
                        col_types = cols(.default = col_character())) %>%
  mutate(scene_category = recode(scene_category, !!!c('man-made' = 'man',
                                                      'natural' = 'nat')),
         behavior = recode(behavior, !!!c('hit'='H',
                                          'miss/forgotten' = 'M',
                                          'falsealarm' = 'FA',
                                          'correctreject' = 'CR')),
         subsequent_correct = recode(subsequent_correct, !!!c('subsequent_forgotten' = 'fgt',
                                                              'subsequent_remembered' = 'rem'))) %>%
  unite(Scene_Cat,2:5)

fs <- list.files(dirRaw, pattern = 'sub.*_events.tsv', recursive = T, full.names = T)

i <- 0
events <- list()
for (f in fs){
  i <- i +1
  events[[i]] <- read_tsv(f,
                   col_types = cols(.default = col_character())) %>%
    left_join(TriggerTable, by = c('value' = 'trigger')) %>%
    mutate(trial_type = Scene_Cat) %>%
    select(-Scene_Cat) %>%
    write_tsv(file = f) %>%
    mutate(sub = str_match(f,'/sub-(\\d{3})_')[,2])
  }

events <- bind_rows(events) %>% 
  group_by(sub) %>% 
    filter(!value %in% c('n/a','boundary')) %>%
  mutate(onset = suppressWarnings(as.numeric(onset)),
         duration = suppressWarnings(as.numeric(duration)),
         sample = suppressWarnings(as.numeric(sample)),
         response_time = suppressWarnings(as.numeric(response_time)),
         lagged_onset = lag(onset),
         ITI = onset - lagged_onset,
         trial_type = as.factor(trial_type),
         value = as.numeric(value),
         sub = as.factor(sub)) %>%
  select(-response_time, -duration, -lagged_onset, -stim_file)

skimr::skim(ungroup(events))
```
## Count trials

```{r}
events %>%
  separate(trial_type,into = c('Scene_Cat','Old_New','SDT','Subsequent_Memory')) %>% 
  mutate(stim = recode(SDT, H = 'Old', M = 'Old', FA = 'New', CR = 'New', na = 'NA')
           ) %>%
  group_by(sub,stim) %>%
  count() %>%
  pivot_wider(id_cols = sub, values_from = n, names_from = c(stim), values_fn = sum)
```



## Showing ITI

```{r}
ggplot(events,aes(x=ITI, fill = trial_type)) +
  geom_histogram(bins = 50) +
  # facet_wrap(~sub) +
  scale_x_log10() +
  ggtitle('ITI for each trial type')
```

## Too long ITIs

ITIs beyond 6.7s mean that subjects took more than 5s to respond. We decide to discard these trials.

```{r}
toolong <- events %>% group_by(sub) %>%
  filter(ITI > 6.7) 

cat('Proportion of too long trials:', sprintf('%0.3f',nrow(toolong) / nrow(events)))

toolong %>% 
  group_by(sub) %>%
  summarize(n_toolong = n(),
            prop = n_toolong/1200) %>%
  flextable::flextable() %>%
  colformat_double(digits = 3)

events <- events %>% mutate(toolong = ITI > 6.7)
```


<!-- ```{r} -->
<!-- ggplot(events,aes(x = ITI, fill = sub)) + -->
<!--   geom_histogram(bins = 30) + -->
<!--   geom_text(aes(x=300, y=0,  fill = NA, label = n), hjust = 1, vjust = -1, data = . %>% group_by(trial_type) %>% summarize(n = n())) + -->
<!--   facet_wrap(~trial_type, scales = 'free_y')+ -->
<!--   scale_x_log10() + -->
<!--   ggtitle('ITI by subject') -->
<!-- ``` -->

## Signal detection parameters

### Natural vs. man-made objects

```{r}

SDT <- events %>% group_by(sub) %>%
  count(trial_type) %>%
  separate(trial_type,into = c('Scene_Cat','Old_New','SDT','Subsequent_Memory')) %>%
  pivot_wider(id_cols = c(sub,Scene_Cat), names_from = SDT, values_from = n, values_fn = sum) %>%
  # pivot_wider(id_cols = sub, names_from = trial_type,values_from = n) %>%
  mutate(HR = H/(H+M), FAR = FA/(FA+CR),
    dp = perf2dp(HR, FAR),
         c = perf2c(HR, FAR))


p0 <- ggplot(SDT,aes(x=FAR,y=HR,col = sub)) +
    geom_point(show.legend = F) +
  coord_equal(xlim = c(0,1), ylim = c(0,1)) +
  ggtitle('ROC points')

  # p1 <- ggplot(SDT,aes(x=Scene_Cat,y=HR,fill = sub)) +
  #   geom_col(position = 'dodge',show.legend = F)
  # 
  # p2 <- ggplot(SDT,aes(x=Scene_Cat,y=FAR,fill = sub)) +
  #   geom_col(position = 'dodge',show.legend = F)
  
  p3 <- ggplot(SDT,aes(x=Scene_Cat,y=dp,col = sub, group = Scene_Cat)) +
    geom_violin(show.legend = F) +
    geom_beeswarm(show.legend = F)
  
  p4 <- ggplot(SDT,aes(x=Scene_Cat,y=c,col = sub, group = Scene_Cat)) +
    geom_violin(show.legend = F) +
    geom_beeswarm(show.legend = F)
  
  # (p1 + p2) /(p3 + p4)
  p0+p3+p4

```

### Subsequently remembered or not

```{r}

SDT <- events %>% group_by(sub) %>%
  count(trial_type) %>%
  separate(trial_type,into = c('Scene_Cat','Old_New','SDT','Subsequent_Memory')) %>%
  pivot_wider(id_cols = c(sub,Subsequent_Memory), names_from = SDT, values_from = n, values_fn = sum) %>%
  # pivot_wider(id_cols = sub, names_from = trial_type,values_from = n) %>%
  mutate(HR = H/(H+M), FAR = FA/(FA+CR),
    dp = perf2dp(HR, FAR),
         c = perf2c(HR, FAR))


p0 <- ggplot(SDT,aes(x=FAR,y=HR,col = Subsequent_Memory)) +
    geom_point(show.legend = F) +
  coord_equal(xlim = c(0,1), ylim = c(0,1)) +
  ggtitle('ROC points')

  # p1 <- ggplot(SDT,aes(x=Scene_Cat,y=HR,fill = sub)) +
  #   geom_col(position = 'dodge',show.legend = F)
  # 
  # p2 <- ggplot(SDT,aes(x=Scene_Cat,y=FAR,fill = sub)) +
  #   geom_col(position = 'dodge',show.legend = F)
  
  p3 <- ggplot(SDT,aes(x=Subsequent_Memory,y=dp,col = Subsequent_Memory, group = Subsequent_Memory)) +
    geom_violin(show.legend = F) +
    geom_beeswarm(show.legend = F)
  
  p4 <- ggplot(SDT,aes(x=Subsequent_Memory,y=c,col = Subsequent_Memory, group = Subsequent_Memory)) +
    geom_violin(show.legend = F) +
    geom_beeswarm(show.legend = F)
  
  # (p1 + p2) /(p3 + p4)
  p0+p3+p4

```

# faire une table comparative selon que toolong inclus ou pas.

```{r}
events
```

Exporter une table pour rejeter les trials toolong vers BIDS.
