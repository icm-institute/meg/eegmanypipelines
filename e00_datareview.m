
e_setup

if bids.validate(dirRaw)
    error('Raw data is messed up!')
end
disp('Ready to go...')

%%
fprintf('Listing raw files...')
files_raw = bids.layout(dirRaw);
fprintf(' done\n')

% %%
% fs = flister('sub-(?<sub>\d{3})_?(?<sess>ses-[^_]+)?_(?<task>task-[^_]+)_(?<type>[^\.]+)(?<ext>.*)','dir',dirRaw);

%% readdata
sub = '002';
decell = @(x)x{1};
dataset = decell(bids.query(files_raw,'data','sub', sub, 'type','eeg'));
cfg = [];
cfg.dataset = dataset;
data = ft_preprocessing(cfg);

%% add elec pos
data.lay = lay;

%% add events
cfg = [];
cfg.dataset = decell(bids.query(files_raw,'data','sub', sub, 'type','events'));
event = ft_read_event(cfg.dataset);

%% define trials
cfg = [];
cfg.dataset = dataset;
cfg.trialdef.eventtype      = unique({event.type});
cfg.trialdef.prestim        = -.5; % in seconds
cfg.trialdef.poststim       = 1.2; % in seconds
cfg = ft_definetrial(cfg);
trl = cfg.trl;
data_cut = ft_redefinetrial(cfg,data);
cfg = [];
cfg.demean = 'yes';
data_cut = ft_preprocessing(cfg,data_cut);
% preproc demean

%% mark blinks
cfg = [];
cfg.artfctdef.eog.channel = 'VEOG';
cfg.artfctdef.eog.cutoff = 2;
% cfg.artfctdef.eog.interactive = 'yes';
cfg.continuous = 'yes';
cfg.trl = trl;
cfg = ft_artifact_eog(cfg,data_cut);
blinks = cfg.artfctdef.eog;

%% mark muscle
cfg = [];
cfg.artfctdef.muscle.channel = chnb({'.*[78]','M.','P9','P10'},data_cut.label);
% cfg.artfctdef.muscle.interactive = 'yes';
cfg.artfctdef.muscle.cutoff = 22;

cfg.continuous = 'yes';
cfg.trl = trl;
cfg = ft_artifact_muscle(cfg,data_cut);
muscle = cfg.artfctdef.muscle;

%% show result
cfg = [];
cfg.method = 'trial';
% cfg.preproc.demean = 'yes';
% cfg.blocksize = 10;
cfg.ylim = [-100 100];
cfg.plotevents = 'yes';
cfg.ploteventlabels = 'no';
cfg.event = event;
cfg.artfctdef.eog = blinks;
cfg.artfctdef.muscle = muscle;
cfg.layout = lay;
data_clean = ft_rejectvisual(cfg,data_cut);

